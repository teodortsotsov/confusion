import React, { Component } from 'react';
import {
	Card,
	CardImg,
	CardText,
	CardBody,
	CardTitle,
	Button,
	Modal,
	ModalHeader,
	ModalBody,
	Form,
	FormGroup, Label, Input, Row, Col
} from 'reactstrap';
import { Breadcrumb, BreadcrumbItem } from 'reactstrap';
import { Link } from 'react-router-dom';
import {Control, Errors, LocalForm} from "react-redux-form";
import { Loading } from './LoadingComponent';
import { baseUrl } from '../shared/baseUrl';
import { FadeTransform, Fade, Stagger } from 'react-animation-components';

const required = (val) => val && val.length;
const maxLength = (len) => (val) => !(val) || (val.length <= len);
const minLength = (len) => (val) => val && (val.length >= len);

class CommentForm extends Component {
	constructor(props) {
		super(props);
		this.toggleModal = this.toggleModal.bind(this);
		this.handleSubmit = this.handleSubmit.bind(this);
		this.state = {
			isModalOpen: false
		};
	}
	toggleModal() {
		this.setState({
			isModalOpen: !this.state.isModalOpen
		});
	}
	handleSubmit(values) {
		this.toggleModal();
		this.props.postComment(this.props.dishId, values.rating, values.author, values.comment);
		// event.preventDefault();
	}
	render() {
		return(
			<div>
				<Button outline onClick={this.toggleModal}><span className="fa fa-pencil fa-lg"></span> Submit Comment</Button>
				<Modal isOpen={this.state.isModalOpen} toggle={this.toggleModal}>
					<ModalHeader toggle={this.toggleModal}>Submit Comment</ModalHeader>
				<ModalBody>
					<LocalForm onSubmit={(values) => this.handleSubmit(values)}>
						<Row>
							<Label htmlFor="rating" md={2}>Rating</Label>
						</Row>
						<Row className="form-group">
							<Col>
								<Control.select model=".rating" name="rating" id="rating"
												className="form-control">
									<option>1</option>
									<option>2</option>
									<option>3</option>
									<option>4</option>
									<option>5</option>
								</Control.select>
							</Col>
						</Row>
						<Row>
							<Label htmlFor="name" md={3}>Your Name</Label>
						</Row>
						<Row className="form-group">
							<Col>
								<Control.text model=".author" id="author" name="author"
											  placeholder="Your Name"
											  className="form-control"
											  validators={{
												  required, minLength: minLength(3), maxLength: maxLength(15)
											  }}
								/>
								<Errors
									className="text-danger"
									model=".name"
									show="touched"
									messages={{
										required: 'Required',
										minLength: 'Must be greater than 2 characters',
										maxLength: 'Must be 15 characters or less'
									}}
								/>
							</Col>
						</Row>
						<Row>
							<Label htmlFor="message" md={2}>Comment</Label>
						</Row>
						<Row className="form-group">
							<Col>
								<Control.textarea model=".comment" id="comment" name="comment"
												  rows="6"
												  className="form-control" />
							</Col>
						</Row>
						<Button type="submit" value="submit" color="primary">Submit</Button>
					</LocalForm>
				</ModalBody>
				</Modal>
			</div>
		);
	}
}

	function RenderDish({dish}) {
		return (
			<FadeTransform
				in
				transformProps={{
					exitTransform: 'scale(0.5) translateY(-50%)'
				}}>
				<Card>
					<CardImg top src={baseUrl + dish.image} alt={dish.name} />
					<CardBody>
						<CardTitle>{dish.name}</CardTitle>
						<CardText>{dish.description}</CardText>
					</CardBody>
				</Card>
			</FadeTransform>
        );
    }

	function RenderComments({comments, postComment, dishId}) {
		
		if(comments != null){
		return (
			<div class="text-left">
				<h4>Comments</h4>
				<ul class="list-unstyled">
					<Stagger in>
						{comments.map((comment) => {
							return (
								<Fade in>
									<li key={comment.id}>
										<p>{comment.comment}</p>
										<p>-- {comment.author} , {new Intl.DateTimeFormat('en-US', { year: 'numeric', month: 'short', day: '2-digit'}).format(new Date(Date.parse(comment.date)))}</p>
									</li>
								</Fade>
							);
						})}
					</Stagger>
				</ul>
				<p>Eat it, just eat it!</p>
				<CommentForm dishId={dishId} postComment={postComment} />
			</div>
		);
		} else{
			return(
				<div></div>
			);
		}
	}
	
	const  DishDetail = (props) => {
		if (props.isLoading) {
			return(
				<div className="container">
					<div className="row">
						<Loading />
					</div>
				</div>
			);
		}
		else if (props.errMess) {
			return(
				<div className="container">
					<div className="row">
						<h4>{props.errMess}</h4>
					</div>
				</div>
			);
		}
		else if (props.dish != null){
		const dish = [props.dish].map((dish) => {
        return (
				<div class="form-group row">
					<div key={dish.id} className="col-12 col-md-5 m-1">
						<RenderDish dish={props.dish} />
					</div>
					<div>
						<RenderComments comments={props.comments} postComment={props.postComment}
						dishId={props.dish.id}/>
					</div>
				</div>
            );
        });


        return (

            <div className="container">
				<div className="row">
					<Breadcrumb>
						<BreadcrumbItem><Link to="/home">Home</Link></BreadcrumbItem>
						<BreadcrumbItem active>Contact Us</BreadcrumbItem>
					</Breadcrumb>
					<div className="col-12">
						<h3>Contact Us</h3>
						<hr />
					</div>
				</div>
                <div className="row">
                    {dish}
                </div>
            </div>
        );
		} else{
			return(
				<div></div>
			);
		}
      
    }

export default DishDetail;